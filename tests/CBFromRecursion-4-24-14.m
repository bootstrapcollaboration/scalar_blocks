(* ::Package:: *)

(* ::Subtitle:: *)
(*Numerical Recursion for Conformal Blocks*)


(* ::Input:: *)
(*"We will use the following representation for intermediate results and conformal blocks:*)
(**)
(*PartialFraction[pol_, {{\[CapitalDelta]1, r1}, {\[CapitalDelta]2, r2}, ... }]*)
(**)
(*which stands for the function*)
(**)
(*pol + \!\(\*FractionBox[\(r1\), \(\[CapitalDelta] - \[CapitalDelta]1\)]\) + \!\(\*FractionBox[\(r2\), \(\[CapitalDelta] - \[CapitalDelta]2\)]\) + ...*)
(**)
(*where pol is a polynomial in \[CapitalDelta].  When we're computing h (as opposed to g), pol will*)
(*be a constant, equal to the vector of derivatives for h_infinity.  During recursion,*)
(*pol and the residues ri will be vectors whose number of components is equal to the*)
(*number of derivatives.  At the end, we'll produce a conformal block, which will be*)
(*represented as a vector of separate PartialFractions whose residues are numbers.";*)
(**)


(* ::Subsubtitle:: *)
(*Setup and Utility Functions*)


prec=200;
half=SetPrecision[1/2,prec];
(* The value of r at the crossing symmetric point *)
rCrossing=SetPrecision[3-2Sqrt[2],prec];

(* Derivatives with respect to r and \[Eta] (respectively)
  corresponding to nmax. Note that Max[m] was here shifted to 2nmax-1
  rather than 2nmax compared to the earlier file. *)
mnDerivSet[nmax_]:=Flatten[Table[{m,n},{m,0,2 nmax-1},{n,0,(2nmax-1-m)/2}],1]

(* Turn a vector of derivatives into a set of rules of the form
  rcDeriv[m,n]\[Rule]x *)
vectorToRules[v_,nmax_]:=Rule@@#&/@zip[rcDeriv@@#&/@mnDerivSet[nmax],v];

(* Identity matrix as a SparseArray *)
identitySparseArray[dim_]:=SparseArray[{i_,i_}->1,{dim,dim},0];

(* Apply substitution rules to a sparse array *)
sparseArraySubst[ar_,rules_]:=SparseArray[ArrayRules[ar]/.rules];

(* A matrix (represented as a SparseArray) representing the action of
  multiplication by r on a set of (r,\[Eta]) derivatives *)
rMatrix[derivs_]:=
        Module[{exprs,basis},
               exprs=Table[D[r f[r,\[Eta]],{r,mn[[1]]},{\[Eta],mn[[2]]}],{mn,derivs}];
               basis=Table[D[ f[r,\[Eta]],{r,mn[[1]]},{\[Eta],mn[[2]]}],{mn,derivs}];
               CoefficientArrays[exprs,basis][[2]]
        ];

(* A function taking k to a matrix representing the action of r^k on a
  set of (r,\[Eta]) derivatives.  The matrices are precomputed and
  stored for all k=0,1,...,order *)
rMatrices[derivs_,order_,r0_]:=
        Module[{rM,id,rMs},
               rM=sparseArraySubst[rMatrix[derivs],r->r0];
               id=identitySparseArray[Length[rM]];
               rMs=NestList[rM.#&,id,order];
               Function[k,rMs[[k+1]]]
        ];

(* A matrix representing the action of multiplication by
  r^\[CapitalDelta] on a set of (r,\[Eta]) derivatives, with an overall
  factor of r^\[CapitalDelta] stripped off. *)
r\[CapitalDelta]Matrix[derivs_]:=
        Module[{exprs,basis},
               exprs=Table[r^-\[CapitalDelta] D[r^\[CapitalDelta] f[r,\[Eta]],
                                                {r,mn[[1]]},
                                                {\[Eta],mn[[2]]}],
                           {mn,derivs}];
               basis=Table[D[ f[r,\[Eta]],{r,mn[[1]]},{\[Eta],mn[[2]]}],{mn,derivs}];
               CoefficientArrays[exprs,basis][[2]]
];

(* Evaluate a partial fraction at \[CapitalDelta]=a *)
evalPartialFraction[PartialFraction[pol_,rs_],\[CapitalDelta]_,a_]:=(pol/.\[CapitalDelta]->a)+(1/(a-(First/@rs))).(Last/@rs);

(* Do the replacement \[CapitalDelta]\[Rule]\[CapitalDelta]+a in a
  partial fraction *)
shiftPartialFraction[PartialFraction[pol_,rs_],\[CapitalDelta]_,a_]:=
        PartialFraction[pol/.\[CapitalDelta]->\[CapitalDelta]+a,
                        Table[{r[[1]]-a,r[[2]]},{r,rs}]];

(* Computes the coefficient of x^m in a series obtained by Series[f[x],{x,0,k}] *)
seriesCoefficient[HoldPattern[SeriesData[_,_,coeffs_,nmin_,nmax_,_]],m_]:=
        Module[{i=m-nmin+1},
               If[i<=0,0,coeffs[[m-nmin+1]]]
        ];

(* zip two lists together *)
zip[xs_,ys_]:=Transpose[{xs,ys}];

(* Transpose a k by n matrix where k might be zero *)
transposeSafe[{},n_]:=Table[{},{i,n}];
transposeSafe[l_,n_]:=Transpose[l];


(* ::Subsubtitle:: *)
(*Initial Condition*)

(* The initial condition for recursion, as a list of PartialFraction's
  for L=0,...,LMax *)
hInfinity[LMax_,\[Nu]_,\[CapitalDelta]12_,\[CapitalDelta]34_,nmax_]:=
        Module[{r0=SetPrecision[3-2Sqrt[2],prec],h0,hL,r,\[Eta],dr,d\[Eta]},
               h0=(1-r^2)^-\[Nu] (1+r^2-2 r \[Eta])^(1/2 (-1+\[CapitalDelta]12-\[CapitalDelta]34)) (1+r^2+2 r \[Eta])^(1/2 (-1-\[CapitalDelta]12+\[CapitalDelta]34))/.{
                       r->Series[r0+dr,{dr,0,2nmax}],
                       \[Eta]->Series[1+d\[Eta],{d\[Eta],0,nmax}]
                                                                                                                                                                     };
               Table[( 
                       hL=h0*GegenbauerC[L,\[Nu],1+d\[Eta]]*L!/Pochhammer[2\[Nu],L];
                       PartialFraction[Table[
                               mn[[1]]!mn[[2]]!seriesCoefficient[seriesCoefficient[hL,mn[[1]]],mn[[2]]],
                               {mn,mnDerivSet[nmax]}
                                       ],{}]
                       ),{L,0,LMax}]
        ];



(* ::Subsubtitle:: *)
(*Recursion Relation*)


(* Computes a list of PoleSeries for L=0,...,LMax representing h_0,
  ..., h_LMax at the n-th level of recursion. *)
cbRecurse[order_,\[Nu]_,\[CapitalDelta]12_,\[CapitalDelta]34_,nmax_,LMax_]:=
        Module[{h,abcSubst,rM,\[Alpha],\[Beta],\[Gamma],maxPoleOrder,blocks,LMaxRecursion},

               rM=rMatrices[mnDerivSet[nmax],order,rCrossing];

               \[Alpha][n_,L_]:=
        -((4^n n)/(n!)^2) Pochhammer[1/2 (1-n+\[CapitalDelta]12),n] Pochhammer[1/2 (1-n+\[CapitalDelta]34),n] Pochhammer[2\[Nu]+L,n]/Pochhammer[\[Nu]+L,n];

               \[Beta][k_,L_]:=
        -(4^(2 k) k (-1)^k Pochhammer[-k+\[Nu],2 k] Pochhammer[1/2 (1-k+L-\[CapitalDelta]12+\[Nu]),k] Pochhammer[1/2 (1-k+L+\[CapitalDelta]12+\[Nu]),k] Pochhammer[1/2 (1-k+L-\[CapitalDelta]34+\[Nu]),k] Pochhammer[1/2 (1-k+L+\[CapitalDelta]34+\[Nu]),k])/((k!)^2 Pochhammer[-k+L+\[Nu],2 k] Pochhammer[1-k+L+\[Nu],2 k]);

               \[Gamma][n_,L_]:=
        -((4^n n)/(n!)^2) Pochhammer[1/2 (1-n+\[CapitalDelta]12),n] Pochhammer[1/2 (1-n+\[CapitalDelta]34),n] Pochhammer[1+L-n,n]/Pochhammer[1+\[Nu]+L-n,n];

               (* To compute blocks up to LMax, we must compute all
               spins up to LMax+order, with decreasing accuracy *)
               LMaxRecursion=LMax+order;
               maxPoleOrder[L_]:=Min[order,LMaxRecursion-L];

               (* Initial condition for the recursion *)
               blocks=hInfinity[LMaxRecursion,\[Nu],\[CapitalDelta]12,\[CapitalDelta]34,nmax];

               h[\[CapitalDelta]0_,l_]:=
        evalPartialFraction[blocks[[l+1]],\[CapitalDelta],\[CapitalDelta]0];

               WriteString["stdout", "Out of ", order, ": "];
               Do[
                       WriteString["stdout", recursionSteps, ", "];
                       blocks=Table[
                               PartialFraction[
                                       blocks[[L+1,1]],
                                       Join[
                                               Table[{-(n+L-1),\[Alpha][n,L]rM[n].h[1-L,L+n]},
                                                     {n,maxPoleOrder[L]}],
                                               Table[{-(-\[Nu]+k-1),\[Beta][k,L]rM[2k].h[1+\[Nu]+k,L]},
                                                     {k,maxPoleOrder[L]/2}],
                                               Table[{-(-L-2\[Nu]+n-1),\[Gamma][n,L]rM[n].h[L+2\[Nu]+1,L-n]},
                                                     {n,Min[maxPoleOrder[L],L]}]
                                       ]
                               ]
                              ,
                                {L,0,LMaxRecursion}
                              ],
                       {recursionSteps,order}
               ];
               Take[blocks,LMax+1]
        ];

(* To get g from h, we must multiply by r^\[CapitalDelta].  This
  function performs that transformation on the PoleSeries of
  derivatives, resulting in a list of PartialFractions. (Derivatives of
  r^\[CapitalDelta] result in polynomials in \[CapitalDelta], which is
  why we have PartialFractions.) *)
restoreR\[CapitalDelta][PartialFraction[const_,poles_],derivs_,r0_]:=
        Module[{r\[CapitalDelta]M,pols,poleProducts},
               r\[CapitalDelta]M=Normal[r\[CapitalDelta]Matrix[derivs]]/.r->r0;
               poleProducts=Table[
                       PolynomialQuotientRemainder[#,
                                                   \[CapitalDelta]-p[[1]],
                                                   \[CapitalDelta]]&/@(r\[CapitalDelta]M.p[[2]])
                            ,{p,poles}
                            ];
               pols=r\[CapitalDelta]M.const+Total[First/@#&/@poleProducts];
               Thread[PartialFraction[pols,zip[First/@poles,#]&/@transposeSafe[Last/@#&/@poleProducts,Length[pols]]]]
        ];

(* Compute a list of blocks g_0, ..., g_LMax, where each block is a
  vector of PartialFractions.  Uses the recursion relation for k
  \[LessEqual] order, so should give results accurate to within r^order
 *)
computeBlocks[LMax_,\[Nu]_,\[CapitalDelta]12_,\[CapitalDelta]34_,nmax_,order_]:=
        restoreR\[CapitalDelta][#,
                                mnDerivSet[nmax],
                                rCrossing]&/@
                               cbRecurse[order,
                                         \[Nu],
                                         \[CapitalDelta]12,
                                         \[CapitalDelta]34,
                                         nmax,
                                         LMax];


(* ::Subsubtitle:: *)
(*Testing*)


(* ::Subtitle:: *)
(*Polynomial Truncation*)


(* This version of truncation attempts to approximate discarded poles
  in terms of kept poles.  The approximation is exact at x=0 (the
  unitarity bound for spin l \[GreaterEqual] 1, and at x=Infinity, and
  in general is better by a factor of 10^-2 or 10^-3 than naively
  discarding a pole. *)
shiftPoles[from_,to_]:=
        Module[{n=Length[to]},
               zip[to,LinearSolve[
                       Table[SetPrecision[to[[i]],prec]^m,{m,-Floor[n/2],n-1-Floor[n/2]},{i,n}],
                       Table[Sum[p[[2]]SetPrecision[p[[1]],prec]^m,{p,from}],{m,-Floor[n/2],n-1-Floor[n/2]}]
                      ]]
        ];

togetherWithFactors[factors_,x_][PartialFraction[poly_,rs_]]:=
        Module[{mult},
               mult=Product[x-f,{f,factors}];
               N[poly*mult+Sum[r[[2]]*Product[x-f,
                                              {f,
                                               Complement[factors,
                                                          {r[[1]]}]}],
                               {r,rs}],
                 prec]//Expand
        ];

(* If a pole p satisfies protectPole[p]=True, leave its residue
  untouched (neither shift the pole elsewhere, nor shift other poles to
  it), meanwhile shift all other poles in the given partial fraction to
  polesToKeep *)
polynomialTruncate[PartialFraction[polynomial_,polePart_],protectPole_,polesToKeep_,x_]:=
        Module[
                {protected,unprotected,keep,allKeptPoles},
                protected=Select[polePart,protectPole[First[#]]&];
                unprotected=Select[polePart,!protectPole[First[#]]&];
                keep=Select[polesToKeep,!protectPole[#]&];
                allKeptPoles=Join[protected,shiftPoles[unprotected,keep]];
                togetherWithFactors[First/@allKeptPoles,x][PartialFraction[polynomial, allKeptPoles]]
        ];

(* The location of the poles in \[CapitalDelta] in a conformal block,
  where order specifies how many poles *)
cbPoles[\[Nu]_,L_,order_]:=
        Join[
                Table[-(n+L-1),{n,order}],
                Table[-(-\[Nu]+k-1),{k,(order)/2}],
                Table[-(-L-2\[Nu]+n-1),{n,Min[order,L]}]
        ];

(* Compute polynomial approximations for the components of a conformal
  block, where we keep poles up to the order keptPoleOrder.  The
  resulting blocks are shifted over by L+2\[Nu]. We protect the pole at
  the unitarity bound, so that its residue remains exact. *)
blockPolynomials[block_,\[Nu]_,L_,keptPoleOrder_]:=
        Module[{keep=#-(L+2\[Nu])&/@cbPoles[\[Nu],
                                            L,
                                            keptPoleOrder],
                protect=Function[p,p==If[L==0,-\[Nu],0]]
               },
               polynomialTruncate[shiftPartialFraction[#,
                                                       \[CapitalDelta],
                                                       L+2\[Nu]],
                                  protect,
                                  keep,
                                  \[CapitalDelta]]&/@block
        ];

(* compute blockPolynomials for each spin L *)
blockTablePolynomialsShifted[blocks_,\[Nu]_,keptPoleOrder_]:=
        Table[blockPolynomials[blocks[[L+1]],
                               \[Nu],
                               L,
                               keptPoleOrder]/.\[CapitalDelta]->x,
              {L,0,Length[blocks]-1}
        ];


(* ::Subsubtitle:: *)
(*Computing transition table between z,zb derivatives and r,c derivatives*)


(* The starting point is an expression for our block as

g = \!\(
\*SubscriptBox[\(\[Sum]\), \(m, n\)]\(
\*FractionBox[\(rcDeriv[m, n]\), \(\(m!\)\(n!\)\)]\[Rho][1/2]
\*SuperscriptBox[\((\[Lambda]\ \[Lambda]b\  - \ 1)\), \(n\)]
\*SuperscriptBox[\((
\*FractionBox[\(1\), \(2\)]\((
\*FractionBox[\(\[Lambda]\), \(\[Lambda]b\)] + 
\*FractionBox[\(\[Lambda]b\), \(\[Lambda]\)])\) - 1)\), \(m\)]\[IndentingNewLine]\[IndentingNewLine]
The\ last\ quantity\ in\ parentheses\ is\ proportional\ to\ a\ square\)\), so both powers can be simplified using the binomial theorem.
It then suffices to compute derivatives of \[Lambda]^p at z=1/2.
We define \[Alpha][a,p]=Subscript[\[PartialD], z]^a\[Lambda]^p.  Then Subscript[\[PartialD], z]^aSubscript[\[PartialD], zb]^bg has the expression

Overscript[Subscript[\[Sum], n=0], a+b]Overscript[Subscript[\[Sum], m=0], (a+b-n)/2]rcDeriv[n,m]/(m!n!)\[Rho][half]^n/2^mOverscript[Subscript[\[Sum], k=0], 2m]Overscript[Subscript[\[Sum], j=0], n]Binomial[n,j]Binomial[2m,k](-1)^(n-j+k)\[Alpha][a,k-m+j]\[Alpha][b,m-k+j]
*)

\[Rho][z_]:=z/(1+Sqrt[1-z])^2;
\[Lambda][z_]:=Sqrt[\[Rho][z]/\[Rho][1/2]];

Clear[\[Lambda]Series];
\[Lambda]Series[order_]:=\[Lambda]Series[order]=Series[\[Lambda][z],{z,half,order}];

Clear[\[Alpha]Series];
\[Alpha]Series[p_,order_]:=
        \[Alpha]Series[p,order]=If[
                p==0,
                Prepend[ConstantArray[0,order],1],
                (\[Lambda]Series[order]^p)[[3]]
                                ];

zzbDerivExpr[a_,b_]:=
        Module[{\[Alpha]},
               \[Alpha][q_,p_]:=q!\[Alpha]Series[p,Max[a,b]][[q+1]];
               Sum[
                       rcDeriv[n,m]/(m!n!) \[Rho][half]^n/2^m Sum[
                               Binomial[n,j]Binomial[2m,k](-1)^(n-j+k) \[Alpha][a,k-m+j]\[Alpha][b,m-k+j],
                               {k,0,2m},
                               {j,0,n}
                                                              ],
                       {n,0,a+b},
                       {m,0,(a+b-n)/2}
               ]
        ];

zzbToRc[nMax_]:=
        Flatten[Table[
                zzbDeriv[a,b]->zzbDerivExpr[a,b],
                {a,0,2nMax},
                {b,0,Min[a,2nMax-1-a]}
                ],1];

(* ::Subtitle:: *)
(*Outputting Tables*)

floatToString[x_]:=If[Abs[x]<10^-10,
                      "0",
                      ToString[CForm[SetAccuracy[x+5*10^(-16),10]]]];
(* Turn off the warning that Monitor gives if we're calling mathematica from the command line *)
Off[FrontEndObject::notavail];

(* For backwards compatibility, you can pass in a single keptPoleOrder *)
WriteRCTable[dir_,
                dim_,
                \[CapitalDelta]\[CapitalDelta]12_,
                \[CapitalDelta]\[CapitalDelta]34_,
                Lmax_,
                nmax_,
                keptPoleOrder_,
                order_]:=
        WriteRCTable[dir,
                     dim,
                     \[CapitalDelta]\[CapitalDelta]12,
                     \[CapitalDelta]\[CapitalDelta]34,
                     Lmax,
                     nmax,
                     {keptPoleOrder},
                     order];

(* The general function allows a list of keptPoleOrders, and it'll create tables for all of them *)
(* Accuracy of blocks goes like 10^(-1.3*keptPoleOrder) *)
(* Correctness of coefficients in polynomial truncations goes like 10^(-0.67*order) *)
(* Precision of numbers goes like 10^(-0.5*keptPoleOrder) *)
WriteRCTable[dir_,
                dim_,
                \[CapitalDelta]\[CapitalDelta]12_,
                \[CapitalDelta]\[CapitalDelta]34_,
                Lmax_,
                nmax_,
                keptPoleOrders_List,order_]:=
        Module[{blocks,
                shiftedblocksPols,
                blockderivmap,
                file,
                d=Rationalize[dim,10^-6],
                \[CapitalDelta]12=SetPrecision[\[CapitalDelta]\[CapitalDelta]12,prec],
                \[CapitalDelta]34=SetPrecision[\[CapitalDelta]\[CapitalDelta]34,prec]},
               Print["Blocks"];
               blocks=Monitor[computeBlocks[Lmax,
                                            (d-2)/2,
                                            \[CapitalDelta]12,
                                            \[CapitalDelta]34,
                                            nmax,order],
                              L];
               Do[
                       
                       shiftedblocksPols=blockTablePolynomialsShifted[blocks,(d-2)/2,keptPoleOrder];

                       blockderivmap=Table[vectorToRules[shiftedblocksPols[[L+1]],nmax],{L,0,Lmax}];

                       file=dir<>
                               "/rcDerivTable-d"<>
                               ToString[N[d]]<>
                               "-delta12-"<>
                               floatToString[\[CapitalDelta]\[CapitalDelta]12]<>
                               "-delta34-"<>
                               floatToString[\[CapitalDelta]\[CapitalDelta]34]<>
                               "-Lmax"<>
                               ToString[Lmax]<>
                               "-nmax"<>
                               ToString[nmax]<>
                               "-keptPoleOrder"<>
                               ToString[keptPoleOrder]<>
                               "-order"<>
                               ToString[order]<>
                               ".m";
                       WriteString["stdout","\n Writing file: ",file," ... "];
                       Put[blockderivmap,file];
                       WriteString["stdout","Done."];
                     ,
                       {keptPoleOrder,keptPoleOrders}
               ];
               file
        ]

dir="tests/output";
WriteRCTable[dir,3,SetPrecision[8791,prec]/10000,SetPrecision[-8791,prec]/10000,21,6,6,5];
