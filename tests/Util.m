highPrecisionToString[x_] := Module[
    {digits = 32, padding = 10, s},
    s = StringTrim[
        StringInsert[
            StringJoin[
                Sequence[Table["0", {i, digits + 1}]],
                ToString[
                    Quotient[Round[10^(digits + padding) Abs[x]], 10^padding]]],
            ".", -(digits + 1)],
        "0" ..];
    If[s == ".", "0",
       StringJoin[
           If[x < 0, "-", ""], 
           If[StringTake[s, 1] == ".",
              StringJoin["0", s],
              s]]]];

zzbDerivTablePath[tableDir_, d_, delta12_, delta34_, L_, nmax_, keptPoleOrder_, order_] := StringJoin[
    tableDir,
    "/nmax",            ToString[nmax],
    "/zzbDerivTable-d", highPrecisionToString[d],
    "-delta12-",        highPrecisionToString[delta12],
    "-delta34-",        highPrecisionToString[delta34],
    "-L",               ToString[L],
    "-nmax",            ToString[nmax],
    "-keptPoleOrder",   ToString[keptPoleOrder],
    "-order",           ToString[order],
    ".m"];

safeExport[file_, expr_] := Module[
    {dir = DirectoryName[file]},
    If[!DirectoryQ[dir],
       If[ $OperatingSystem == "Unix",
           Run["mkdir -p -m 777 "<>dir];,
           CreateDirectory[dir,CreateIntermediateDirectories->True];
       ];
    ];
    Export[file, expr];
    If[ $OperatingSystem == "Unix",
        Run["chmod a+rw "<>file];
    ];
];

