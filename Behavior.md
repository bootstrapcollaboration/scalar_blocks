# Technical details about behavior of `scalar_blocks`

- All non-integer input parameters are interpreted as exact values in base 10. 
  I.e. `0.1234` is interpreted as `0.1234000...`. 
- All non-integer input parameters that are forwarded to the output file names
  are forwarded exactly. For example, if one uses `--delta-12=1`, the 
  output file name will contain `-delta12-1-`. But if one uses `--delta-12=1.000`,
  the output file name will now contain `-delta12-1.000-`.
- Parameter `--dim` can be non-integer. If it is an even integer (and this includes both
  `--dim=4` and `--dim=4.000`, etc.), a special code is used to compute the blocks.
  It is thus not guaranteed (although may be true) that the output for `--dim=3.9999`
  will be close to the output for `--dim=4`.
- The code may not work properly for dimensions less than 2 (fractional and integer alike).
- The code performs "pole-shifting" as discussed in appendix A of 
  [arXiv:1307.6856](https://arxiv.org/abs/1307.6856). The poles at the unitarity
  bound are unaffected by this. If pole-shifting is not desired, one may set
  `poles` and `order` to be the same number, in which case no pole-shifting will
  be performed, e.g. `--poles=90 --order=90`.