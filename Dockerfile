# syntax=docker/dockerfile:1

# This file contains three stages: 'build', 'install', 'test'
# https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#use-multi-stage-builds
#
# 'build' builds scalar_blocks binaries. This image is heavy, because it contains all sources etc.
# 'install' contains only scalar_blocks binaries (in /usr/local/bin/) + necessary dynamic libraries
# 'test' is based on 'install', but also copies scalar_blocks/build/ and scalar_blocks/test/ folders to /home/testuser/scalar_blocks/
# - this allows to in order to run unit and integration tests by calling scalar_blocks/test/run_all_tests.sh
# Final (lightweight) image is made from 'install' target.
#
# How to check that scalar_blocks image works:
#
# docker build . --tag scalar_blocks
# docker run scalar_blocks scalar_blocks --help
#
# How to run tests (TODO currently they don't check output correctness):
#
# docker build . --tag scalar_blocks-test --target test
# docker run scalar_blocks-test ./tests/create_test_tables.sh
#
# Note: 'mpirun --oversubscribe' is necessary only if your environment has less than 6 CPUs available

# Trilinos binaries built from https://github.com/trilinos/Trilinos
# based on the same alpine:3.18 build as below

FROM vasdommes/trilinos-sacado:14.4.0 AS build

RUN apk add \
    boost-dev \
    eigen-dev \
    g++ \
    git \
    gmp-dev \
    python3
WORKDIR /usr/local/src/scalar_blocks
# Build scalar_blocks from current sources
COPY . .
RUN ./waf configure --prefix=/usr/local && \
    python3 ./waf && \
    python3 ./waf install

# Take only scalar_blocks binaries + load necessary dynamic libraries
FROM alpine:3.18 as install
RUN apk add \
    boost1.82-program_options \
    boost1.82-system \
    eigen \
    gmp \
    libgmpxx \
    libstdc++
COPY --from=build /usr/local/bin /usr/local/bin
COPY --from=build /usr/local/lib /usr/local/lib

# Separate test target, see
# https://docs.docker.com/language/java/run-tests/#multi-stage-dockerfile-for-testing
# Contains /home/testuser/scalar_blocks/build and /home/testuser/scalar_blocks/test folders,
# which is sufficient to run tests as shown below:
#
# docker build . --tag scalar_blocks-test --target test
# docker run scalar_blocks-test ./tests/create_test_tables.sh
FROM install as test
# Create testuser to run Docker non-root
RUN addgroup --gid 10000 testgroup && \
    adduser --disabled-password --uid 10000 --ingroup testgroup --shell /bin/sh testuser
WORKDIR /home/testuser/scalar_blocks
COPY --from=build /usr/local/src/scalar_blocks/build build
COPY --from=build /usr/local/src/scalar_blocks/tests tests
RUN chown -R testuser tests
USER testuser:testgroup

# Resulting image
FROM install
# TODO best practices suggest to run containter as non-root.
# https://github.com/dnaprawa/dockerfile-best-practices#run-as-a-non-root-user
# But this requires some extra work with permissions when mounting folders for docker run.