# Harvard FAS RC

- [Harvard FAS RC User Guide](https://docs.rc.fas.harvard.edu/)

## Load modules

    module load python gcc/12.2.0-fasrc01 cmake/3.25.2-fasrc01 gmp/6.2.1-fasrc01 

You may run `module -t list` to view loaded modules,
and `module purge` to unload all modules.

## Install

### Trilinos

    git clone --branch trilinos-release-14-4-branch https://github.com/trilinos/Trilinos.git
    cd Trilinos
    mkdir build
    cd build
    cmake .. -DCMAKE_CXX_COMPILER=g++ -DCMAKE_C_COMPILER=gcc -DTrilinos_ENABLE_Sacado=ON -DTrilinos_ENABLE_Kokkos=OFF -DTrilinos_ENABLE_Teuchos=OFF -DCMAKE_INSTALL_PREFIX=$HOME/install
    make && make install
    cd ../..

### scalar_blocks

    git clone https://gitlab.com/bootstrapcollaboration/scalar_blocks.git
    cd scalar_blocks
    ./waf configure --prefix=$HOME/install --trilinos-dir=$HOME/install --eigen-incdir=/software/eigen-b3f3d4950030/
    ./waf -j 1
    ./tests/create_test_tables.sh 
    ./waf install
    cd ..
