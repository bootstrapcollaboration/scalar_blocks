# Caltech HPC

- [Caltech HPC documentation](https://hpc.sites.caltech.edu/documentation)
- [Software and Modules](https://hpc.sites.caltech.edu/documentation/software-and-modules)

## Load modules

    module load git/2.37.2 cmake/3.25.1 gcc/9.2.0 boost/1_81_0_gcc-9.2.0 eigen/eigen

You may run `module -t list` to view loaded modules,
and `module purge` to unload all modules.

## Install

### Trilinos

    git clone --branch trilinos-release-14-4-branch https://github.com/trilinos/Trilinos.git
    cd Trilinos
    mkdir build
    cd build
    cmake .. -DCMAKE_CXX_COMPILER=g++ -DCMAKE_C_COMPILER=gcc -DTrilinos_ENABLE_Sacado=ON -DTrilinos_ENABLE_Kokkos=OFF -DTrilinos_ENABLE_Teuchos=OFF -DCMAKE_INSTALL_PREFIX=$HOME/install
    make && make install
    cd ../..

### scalar_blocks

    git clone https://gitlab.com/bootstrapcollaboration/scalar_blocks.git
    cd scalar_blocks
    ./waf configure --prefix=$HOME/install --trilinos-dir=$HOME/install --eigen-incdir=/software/eigen-b3f3d4950030/
    ./waf -j 1
    ./tests/create_test_tables.sh
    ./waf install
    cd ..
