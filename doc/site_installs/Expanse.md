# Expanse HPC

- [Expanse HPC User Guide](https://www.sdsc.edu/support/user_guides/expanse.html)

## Load modules

    module load cpu/0.15.4 intel/19.1.1.217 eigen/3.3.7 gcc/10.2.0 cmake/3.18.2 boost/1.74.0 gmp/6.1.2

You may run `module -t list` to view loaded modules,
and `module purge` to unload all modules.

## Install

### Trilinos

    git clone --branch trilinos-release-13-4-branch https://github.com/trilinos/Trilinos.git
    cd Trilinos
    mkdir build
    cd build
    cmake .. -DCMAKE_CXX_COMPILER=g++ -DCMAKE_C_COMPILER=gcc -DCMAKE_Fortran_COMPILER=gfortran -DTrilinos_ENABLE_Sacado=ON -DTrilinos_ENABLE_Kokkos=OFF -DTrilinos_ENABLE_Teuchos=OFF -DCMAKE_INSTALL_PREFIX=$HOME/install
    make && make install
    cd ..

### scalar_blocks

    git clone https://gitlab.com/bootstrapcollaboration/scalar_blocks
    cd scalar_blocks
    ./waf configure --trilinos-dir=$HOME/install --eigen-incdir=/cm/shared/apps/spack/cpu/opt/spack/linux-centos8-zen2/intel-19.1.1.217/eigen-3.3.7-plaog3szjnn3gh6wq5co55xxjuswwo7f/include/eigen3 --prefix=$HOME/install
    ./waf
    ./tests/create_test_tables.sh
    ./waf install
    cd ..
