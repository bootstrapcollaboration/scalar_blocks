* [Using Docker](#Use-Docker)
* [Compiling from sources](#Compiling-from-source)

# Using Docker

The easiest way is to download the latest [Docker image](https://hub.docker.com/r/bootstrapcollaboration/scalar_blocks):

    docker pull bootstrapcollaboration/scalar_blocks:master

and run it as follows:

    docker run -v $PWD:/usr/local/share/scalar_blocks bootstrapcollaboration/scalar_blocks:master scalar_blocks --dim 3 --order 5 --max-derivs 6 --spin-ranges 0-21 --poles 6 --delta-12 0.8791  --delta-34 0.8791 --num-threads=4 -o /usr/local/share/scalar_blocks/output --precision=665

This command will [mount](https://docs.docker.com/storage/bind-mounts/) your current directory to `/usr/local/share/scalar_blocks` directory of the image and run `scalar_blocks` with the given arguments.

You may also [build](https://docs.docker.com/engine/reference/commandline/build/) Docker image by yourself, using the included [Dockerfile](Dockerfile):

    docker build . --tag scalar_blocks:my_tag
    docker run scalar_blocks:my_tag scalar_blocks --help

# Compiling from source

This is a general guide for building `scalar_blocks`.
Site-specific notes for different HPC machines cna be found in [doc/site_installs](doc/site_installs) folder.

## Requirements

Scalar Blocks requires

- A modern C++ compiler with C++ 17 support.
Scalar Blocks has been tested with GCC 9.2.0, 10.2.0, and 12.2.0.

- [Boost C++ Libraries](http://www.boost.org/) Please be sure that the
  boost library is compiled with the same C++ compiler you are using.
  Scalar Blocks has been tested with version 1.66, 1.67, 1.68, 1.74, 1.81.
  Versions 1.69-1.73 have a performance bug which prevents
  them from performing well with multiple cores.

- [The GNU Multiprecision Library](https://gmplib.org/) This is used
  by the Boost multiprecision library.  Only the C bindings are
  required.

- [Eigen](https://eigen.tuxfamily.org)

- [Python](https://python.org)

- [CMake](https://cmake.org/)

- [Trilinos](https://trilinos.org/) Scalar blocks only uses
  [Sacado](https://trilinos.org/packages/sacado/) for automatic
  differentiation.

Scalar Blocks has only been tested on Linux (Debian buster and Centos
7).  In principle, it should be installable on Mac OS X using 
libraries from a package manager such as [Homebrew](https://brew.sh).

Some of these dependencies may be available via modules or a package
manager.  On Yale's Grace cluster:

    module load Langs/GCC/7.3.0 Libs/Boost Tools/Cmake Libs/GMP Libs/Eigen

On Debian Buster:

    apt-get install libeigen3-dev libtrilinos-sacado-dev libtrilinos-sacado-dev libtrilinos-kokkos-dev libtrilinos-teuchos-dev libboost-dev

## Installation

1. Download the latest release of Trilinos from git

        git clone --branch trilinos-release-14-4-branch https://github.com/trilinos/Trilinos.git
    
2. Make the build directory and cd into it.

        mkdir -p Trilinos/build
        cd Trilinos/build
    
3. Configure Trilinos.  We only need Sacado, so we turn off everything else.

        cmake -DTrilinos_ENABLE_Sacado=ON -DTrilinos_ENABLE_Kokkos=OFF -DTrilinos_ENABLE_Teuchos=OFF -DCMAKE_INSTALL_PREFIX=../../trilinos_bin ..

4. Build and install Trilinos

        make && make install

5. Download Scalar Blocks

        cd ../..
        git clone https://gitlab.com/bootstrapcollaboration/scalar_blocks.git
        cd scalar_blocks

6. Configure the project using the included version of
   [waf](https://waf.io).  Waf can usually find libraries that are in
   system directories, but it needs direction for everything else is.
   If you are having problems, running `python ./waf --help` will give
   you a list of options.
   
   On Yale's Grace cluster, a working command is

        ./waf configure --trilinos-dir=$HOME/project/trilinos_bin

    and on Debian buster, it is

        CXX=mpicxx python ./waf configure

7. Type `python ./waf` to build the executable in `build/scalar_blocks`.  Running
   
        ./build/scalar_blocks --help
   
   should give you a usage message.
