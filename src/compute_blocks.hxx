#pragma once

#include "Delta_Fraction.hxx"
#include "Nu.hxx"
#include "Timers.hxx"

std::vector<std::vector<Delta_Fraction>>
compute_blocks(const std::vector<std::pair<int64_t, int64_t>> &derivs,
               const int64_t &L_max, const Nu &nu, const Bigfloat &Delta_12,
               const Bigfloat &Delta_34, const int64_t &n_max,
               const int64_t &order, const size_t &num_threads,
               const std::string &timer_prefix, Timers &timers);

