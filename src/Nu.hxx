#pragma once

#include "Bigfloat.hxx"

#include <boost/variant.hpp>
#include <string>
#include <functional>

namespace
{
  template <typename Return_Type>
  struct Nu_Visitor : public boost::static_visitor<Return_Type>
  {
    const std::function<Return_Type(const int64_t &)> &int64_t_fn;
    const std::function<Return_Type(const Bigfloat &)> &Bigfloat_fn;
    Nu_Visitor(const std::function<Return_Type(const int64_t &)> &Int64_t_Fn,
               const std::function<Return_Type(const Bigfloat &)> &Bigfloat_Fn)
        : int64_t_fn(Int64_t_Fn), Bigfloat_fn(Bigfloat_Fn)
    {}

    Return_Type operator()(const std::int64_t &nu) const
    {
      return int64_t_fn(nu);
    }
    Return_Type operator()(const Bigfloat &nu) const
    {
      return Bigfloat_fn(nu);
    }
  };
}

struct Nu
{
  boost::variant<int64_t, Bigfloat> variant;
  Nu(const std::string &dim_string);

  template <typename Return_Type>
  Return_Type
  visit(const std::function<Return_Type(const int64_t &)> &int64_t_fn,
        const std::function<Return_Type(const Bigfloat &)> &Bigfloat_fn) const
  {
    Nu_Visitor<Return_Type> visitor(int64_t_fn, Bigfloat_fn);
    return boost::apply_visitor(visitor,variant);
  }

  Bigfloat to_Bigfloat() const
  {
    std::function<Bigfloat(const int64_t &)> int64_fn(
      [](const int64_t &nu_int) { return Bigfloat(nu_int); });
    std::function<Bigfloat(const Bigfloat &)> Bigfloat_fn(
      [](const Bigfloat &nu_Bigfloat) { return nu_Bigfloat; });

    return visit(int64_fn, Bigfloat_fn);
  }
};
