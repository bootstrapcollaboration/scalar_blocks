#pragma once

#include "../Nu.hxx"
#include "../Timers.hxx"

#include <vector>

struct Residues
{
  std::vector<std::vector<std::vector<Bigfloat>>> alpha, beta, gamma, alpha2;
  Residues(std::vector<Bigfloat> &h_infinity, const int64_t &L_max_recursion,
           const int64_t &order, const Nu &nu, const Bigfloat &Delta_12,
           const Bigfloat &Delta_34, const size_t &num_threads,
           const std::string &timer_prefix, Timers &timers);
};
