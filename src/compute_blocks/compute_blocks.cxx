#include "Residues.hxx"
#include "Matrix.hxx"
#include "dot.hxx"
#include "../Delta_Fraction.hxx"
#include "../Timers.hxx"
#include "../Nu.hxx"

#include <list>
#include <thread>

std::vector<Bigfloat>
compute_h_infinity(const Bigfloat &nu, const Bigfloat &Delta_12,
                   const Bigfloat &Delta_34, const int64_t &order);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
r_delta_matrices(const std::vector<std::pair<int64_t, int64_t>> &derivs,
                 const Bigfloat &r_crossing, const int64_t &n_max);

void fill_result(
  const std::vector<Bigfloat> &partial_polynomial, const int64_t &L_max,
  const Bigfloat &nu, const Residues &residues,
  const std::vector<std::pair<int64_t, int64_t>> &derivs,
  const Matrix<Bigfloat> &derivs_around_rho,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &r_delta,
  const size_t &num_threads, const size_t &thread_rank,
  std::vector<std::vector<Delta_Fraction>> &result, Timer &thread_timer);

void fill_result_even_dim(
  const std::vector<Bigfloat> &partial_polynomial, const int64_t &L_max,
  const int64_t &nu, const Residues &residues,
  const std::vector<std::pair<int64_t, int64_t>> &derivs,
  const Matrix<Bigfloat> &derivs_around_rho,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &r_delta,
  const size_t &num_threads, const size_t &thread_rank,
  std::vector<std::vector<Delta_Fraction>> &result, Timer &thread_timer);

std::vector<std::vector<Delta_Fraction>>
compute_blocks(const std::vector<std::pair<int64_t, int64_t>> &derivs,
               const int64_t &L_max, const Nu &nu, const Bigfloat &Delta_12,
               const Bigfloat &Delta_34, const int64_t &n_max,
               const int64_t &order, const size_t &num_threads,
               const std::string &timer_prefix, Timers &timers)
{
  const int64_t L_max_recursion(L_max + order);
  const Bigfloat r_crossing(3 - 2 * sqrt(Bigfloat(2)));

  const std::string residues_timer_name(timer_prefix + "residues");
  Timer &residues_timer(timers.add_and_start(residues_timer_name));
  std::vector<Bigfloat> h_infinity(
    compute_h_infinity(nu.to_Bigfloat(), Delta_12, Delta_34, order));
  Residues &&residues
    = Residues(h_infinity, L_max_recursion, order, nu, Delta_12, Delta_34,
               num_threads, residues_timer_name + ".", timers);
  residues_timer.stop();

  Matrix<Bigfloat> derivs_around_rho(2 * n_max, order + 1);
  for(size_t n = 0; n < derivs_around_rho.row_size; ++n)
    for(size_t m = n; m < derivs_around_rho.column_size; ++m)
      {
        const int64_t diff(m - n);
        derivs_around_rho(n, m)
          = (factorial(m) / factorial(diff)) * pow(r_crossing, diff);
      }

  std::vector<Bigfloat> partial_polynomial(dot(derivs_around_rho, h_infinity));

  Timer &r_delta_timer(timers.add_and_start(timer_prefix + "r_delta"));
  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> r_delta(
    r_delta_matrices(derivs, r_crossing, n_max));
  r_delta_timer.stop();

  const std::string result_timer_name(timer_prefix + "result");
  Timer &result_timer(timers.add_and_start(result_timer_name));
  std::vector<std::vector<Delta_Fraction>> result(L_max + 1);

  std::list<std::thread> threads;
  for(size_t thread_rank = 0; thread_rank < num_threads; ++thread_rank)
    {
      Timer &thread_timer(timers.add_and_start(result_timer_name + ".thread_"
                                               + std::to_string(thread_rank)));

      std::function<void(const int64_t &)> int64_fn(
        [&](const int64_t &nu_int) {
          threads.emplace_back(
            fill_result_even_dim, std::cref(partial_polynomial), L_max,
            std::cref(nu_int), std::cref(residues), std::cref(derivs),
            std::cref(derivs_around_rho), std::cref(r_delta), num_threads,
            thread_rank, std::ref(result), std::ref(thread_timer));
        });
      std::function<void(const Bigfloat &)> Bigfloat_fn(
        [&](const Bigfloat &nu_Bigfloat) {
          threads.emplace_back(
            fill_result, std::cref(partial_polynomial), L_max,
            std::cref(nu_Bigfloat), std::cref(residues), std::cref(derivs),
            std::cref(derivs_around_rho), std::cref(r_delta), num_threads,
            thread_rank, std::ref(result), std::ref(thread_timer));
        });
      nu.visit(int64_fn, Bigfloat_fn);
    }
  for(auto &thread : threads)
    {
      thread.join();
    }
  result_timer.stop();

  return result;
}
