#pragma once

#include <string>
#include <cstdint>
#include <utility>
#include <set>

std::pair<std::set<int64_t>, int64_t>
parse_spin_ranges(const std::string &spin_ranges_string);

