#pragma once

#include "../../Nu.hxx"

#include <vector>

std::vector<std::vector<Bigfloat>>
cb_poles(const Nu &nu, const int64_t &L, const int64_t &order);
