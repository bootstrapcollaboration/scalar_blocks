#include "../../../Delta_Fraction.hxx"

boost::math::tools::polynomial<Bigfloat> together_with_factors(
  const std::vector<std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>>>
    &all_kept_poles,
  const boost::math::tools::polynomial<Bigfloat> &polynomial)
{
  boost::math::tools::polynomial<Bigfloat> product(polynomial);
  for(size_t pole_degree_index(0); pole_degree_index != all_kept_poles.size();
      ++pole_degree_index)
    for(auto &pole : all_kept_poles[pole_degree_index])
      {
        product
          *= pow(boost::math::tools::polynomial<Bigfloat>({-pole.first, 1}),
                 pole_degree_index + 1);
      }
  boost::math::tools::polynomial<Bigfloat> sum(0);
  for(size_t pole_degree_index(0); pole_degree_index != all_kept_poles.size();
      ++pole_degree_index)
    for(size_t index(0); index < all_kept_poles[pole_degree_index].size();
        ++index)
      {
        boost::math::tools::polynomial<Bigfloat> complement;
        for(size_t degree(0);
            degree != all_kept_poles[pole_degree_index][index].second.size();
            ++degree)
          {
            complement
              += all_kept_poles[pole_degree_index][index].second[degree]
                 * pow(boost::math::tools::polynomial<Bigfloat>(
                         {-all_kept_poles[pole_degree_index][index].first, 1}),
                       degree);
          }

        for(size_t complement_pole_degree_index(0);
            complement_pole_degree_index != all_kept_poles.size();
            ++complement_pole_degree_index)
          for(size_t complement_index(0);
              complement_index
              < all_kept_poles[complement_pole_degree_index].size();
              ++complement_index)
            {
              if(pole_degree_index != complement_pole_degree_index
                 || index != complement_index)
                {
                  for(size_t degree(0);
                      degree != complement_pole_degree_index + 1; ++degree)
                    {
                      complement *= boost::math::tools::polynomial<Bigfloat>(
                        {-all_kept_poles[complement_pole_degree_index]
                                        [complement_index]
                                          .first,
                         1});
                    }
                }
            }
        sum += complement;
      }
  return product + sum;
}
