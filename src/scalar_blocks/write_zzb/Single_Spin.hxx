#pragma once

#include "../../Bigfloat.hxx"

#include <boost/math/tools/polynomial.hpp>

#include <utility>
#include <vector>

struct Single_Spin
{
  std::vector<boost::math::tools::polynomial<Bigfloat>> numerator;
  std::vector<std::vector<Bigfloat>> denominators;
};
