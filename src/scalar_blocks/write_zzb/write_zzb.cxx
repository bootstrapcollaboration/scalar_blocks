#include "Single_Spin.hxx"
#include "../../mn_deriv_set.hxx"
#include "../../compute_blocks.hxx"

#include <filesystem>
#include <list>
#include <set>
#include <thread>

std::vector<Single_Spin>
shift_blocks(const std::vector<std::vector<Delta_Fraction>> &blocks,
             const Nu &nu, const int64_t &kept_pole_order,
             const std::vector<std::vector<std::vector<Bigfloat>>> &user_poles,
             const bool &need_to_shift_poles, const size_t &num_threads,
             const std::string &timer_prefix, Timers &timers);

void write_spins(
  const std::filesystem::path &output_dir, const Nu &nu,
  const std::string &dim, const std::string &Delta_12_string,
  const std::string &Delta_34_string, const std::set<int64_t> &spins,
  const Bigfloat &Delta_12, const Bigfloat &Delta_34, const int64_t &n_max,
  const bool &output_ab, const int64_t &kept_pole_order, const int64_t &order,
  const std::vector<Single_Spin> &shifted_blocks,
  const std::vector<std::vector<std::vector<Bigfloat>>> &user_poles,
  const bool &output_poles, const size_t &num_threads,
  const size_t &thread_rank, const bool &debug, Timer &thread_timer);

void write_zzb(
  const std::filesystem::path &output_dir, const Nu &nu,
  const std::string &dim, const std::string &Delta_12_string,
  const std::string &Delta_34_string,
  const std::pair<std::set<int64_t>, int64_t> &spins, const int64_t &n_max,
  const int64_t &kept_pole_order, const int64_t &order,
  const std::vector<std::vector<std::vector<Bigfloat>>> &user_poles,
  const bool &output_ab, const bool &output_poles, const size_t &num_threads,
  const bool &debug, Timers &timers)
{
  std::vector<std::pair<int64_t, int64_t>> derivs(mn_deriv_set(n_max));
  Timer &compute_blocks_timer(timers.add_and_start("compute_blocks"));
  const Bigfloat Delta_12(Delta_12_string), Delta_34(Delta_34_string);
  std::vector<std::vector<Delta_Fraction>> blocks(
    compute_blocks(derivs, spins.second, nu, Delta_12, Delta_34, n_max, order,
                   num_threads, "compute_blocks.", timers));
  compute_blocks_timer.stop();

  create_directories(output_dir);
  {
    // We need this useless formatting so that some global locale
    // variable deep within glibc gets set.  Otherwise, each thread,
    // upon formatting an int for the first time, will each try to set
    // it independently, possibly causing a data race.  Valgrind's
    // Helgrind will see it during a call to
    // std::ctype<char>::_M_widen_init().
    std::stringstream ss;
    ss << 1;
  }

  {
    {
      const std::string shift_timer_name("write_zzb.shift");
      Timer &shift_timer(timers.add_and_start(shift_timer_name));

      const bool need_to_shift_poles(!user_poles.empty()
                                     || kept_pole_order != order);
      std::vector<Single_Spin> shifted_blocks(shift_blocks(
        blocks, nu, kept_pole_order, user_poles, need_to_shift_poles,
        num_threads, shift_timer_name + ".", timers));
      shift_timer.stop();

      std::list<std::thread> threads;
      for(size_t thread_rank = 0; thread_rank < num_threads; ++thread_rank)
        {
          Timer &thread_timer(timers.add_and_start(
            "write_spins.thread_" + std::to_string(thread_rank)));
          threads.emplace_back(
            write_spins, std::cref(output_dir), nu, std::cref(dim),
            std::cref(Delta_12_string), std::cref(Delta_34_string),
            spins.first, std::cref(Delta_12), std::cref(Delta_34), n_max,
            output_ab, kept_pole_order, order, std::cref(shifted_blocks),
            std::cref(user_poles), output_poles, num_threads, thread_rank,
            debug, std::ref(thread_timer));
        }
      for(auto &thread : threads)
        {
          thread.join();
        }
    }
  }
}
