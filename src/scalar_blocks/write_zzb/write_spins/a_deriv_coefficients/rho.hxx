#pragma once

template <typename T>
T rho(const T& z)
{
  return z*pow(1+sqrt(1-z),-2);
}

