#include "../Timers.hxx"
#include "../Nu.hxx"
#include "../parse_spin_ranges.hxx"

#include <boost/program_options.hpp>
#include <filesystem>
#include <vector>

namespace po = boost::program_options;

std::vector<std::vector<std::vector<Bigfloat>>>
parse_user_poles(const std::string &user_poles_string);

void write_zzb(
  const std::filesystem::path &output_dir, const Nu &nu,
  const std::string &dim, const std::string &Delta_12_string,
  const std::string &Delta_34_string,
  const std::pair<std::set<int64_t>, int64_t> &spins, const int64_t &n_max,
  const int64_t &kept_pole_order, const int64_t &order,
  const std::vector<std::vector<std::vector<Bigfloat>>> &user_poles,
  const bool &output_ab, const bool &output_poles, const size_t &num_threads,
  const bool &debug, Timers &timers);

int main(int argc, char *argv[])
{
  try
    {
      po::options_description options("Scalar Blocks options");

      int64_t order, n_max;
      std::string spin_ranges_string;
      size_t num_threads;
      std::string Delta_12, Delta_34, dim, poles_string;
      size_t precision_base_2;
      std::filesystem::path output_directory;
      bool debug(false);
      bool output_ab(false);
      bool output_poles(false);

      options.add_options()("help,h", "Show this helpful message.");
      options.add_options()("version,v", "Show version info.");
      options.add_options()("dim", po::value<std::string>(&dim)->required(),
                            "Dimension");
      options.add_options()("order", po::value<int64_t>(&order)->required(),
                            "Depth of recursion");
      options.add_options()("max-derivs",
                            po::value<int64_t>(&n_max)->required(),
                            "Max number of derivs (n_max)");
      options.add_options()("output-ab", po::bool_switch(&output_ab),
                            "Output a,b derivatvies "
                            "instead of z,zb derivatives.");
      options.add_options()(
        "spin-ranges", po::value<std::string>(&spin_ranges_string)->required(),
        "Comma separated list of ranges of spins to compute "
        "(e.g. 12-16,22,37-49)");
      options.add_options()(
        "delta-12", po::value<std::string>(&Delta_12)->required(), "Δ₁₂");
      options.add_options()(
        "delta-34", po::value<std::string>(&Delta_34)->required(), "Δ₃₄");
      options.add_options()(
        "poles", po::value<std::string>(&poles_string)->required(),
        "Either a single number indicating the pole order to keep, or a set "
        "of user specified poles.  User specified poles are an array of "
        "arrays of arrays with dimensions "
        "[spin (L), degree (single, double, triple, etc.), pole index].  For "
        "example, the input [[[0,-1],[-2.5,-3.5]]] will use single poles for "
        "L=0 at 0 and 1, and double poles at -2.5 and -3.5.  "
        "[[],[[],[],[4.5,5.5]]] "
        "will use no poles for L=0 and triple poles for L=1 at 4.5 and 5.5.");

      options.add_options()("output-poles", po::bool_switch(&output_poles),
                            "Include shifted poles in output");
      options.add_options()("num-threads",
                            po::value<size_t>(&num_threads)->required(),
                            "Number of threads");
      options.add_options()("precision",
                            po::value<size_t>(&precision_base_2)->required(),
                            "The precision, in the number of bits, for "
                            "numbers in the computation.");
      options.add_options()(
        ",o", po::value<std::filesystem::path>(&output_directory)->required(),
        "Output directory");
      options.add_options()("debug",
                            po::value<bool>(&debug)->default_value(false),
                            "Write out debugging output.");

      po::variables_map variables_map;

      po::store(po::parse_command_line(argc, argv, options), variables_map);
      if(variables_map.count("help"))
        {
          std::cout << options << '\n';
          return 0;
        }
      if(variables_map.count("version"))
        {
          std::cout << "Scalar Blocks " << SCALAR_BLOCKS_VERSION_STRING << '\n';
          return 0;
        }
      po::notify(variables_map);

      // Round up the size to work around bugs in boost::multiprecision
      const size_t precision_base_2_rounded_up(
        (((precision_base_2 - 1) / (sizeof(mp_limb_t) * 8)) + 1)
        * sizeof(mp_limb_t) * 8);
      const size_t precision_base_10(
        boost::multiprecision::detail::digits2_2_10(
          precision_base_2_rounded_up));

      boost::multiprecision::mpf_float::default_precision(precision_base_10);

      const Bigfloat r_crossing(3 - 2 * sqrt(Bigfloat(2)));

      std::pair<std::set<int64_t>, int64_t> spins(
        parse_spin_ranges(spin_ranges_string));

      Nu nu(dim);

      Timers timers(debug);
      Timer &total_timer(timers.add_and_start("Total"));
      int64_t kept_pole_order([&]() -> int64_t {
        try
          {
            return std::stoi(poles_string);
          }
        catch(...)
          {}
        return std::numeric_limits<int64_t>::max();
      }());
      std::vector<std::vector<std::vector<Bigfloat>>> user_poles(
        parse_user_poles(kept_pole_order != std::numeric_limits<int64_t>::max()
                           ? std::string()
                           : poles_string));

      write_zzb(output_directory, nu, dim, Delta_12, Delta_34, spins, n_max,
                kept_pole_order, order, user_poles, output_ab, output_poles,
                num_threads, debug, timers);
      total_timer.stop();
      if(debug)
        {
          timers.write_profile(output_directory / "profile");
        }
    }
  catch(std::exception &e)
    {
      std::cout << "Error: " << e.what() << "\n";
      exit(1);
    }
  catch(...)
    {
      std::cout << "Unknown error\n";
      exit(1);
    }
}
