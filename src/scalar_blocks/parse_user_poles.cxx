#include "../Bigfloat.hxx"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <vector>
#include <string>

std::vector<std::vector<std::vector<Bigfloat>>>
parse_user_poles(const std::string &user_poles_string)
{
  std::vector<std::vector<std::vector<Bigfloat>>> result;
  if(!user_poles_string.empty())
    {
      std::stringstream ss;
      ss.str(user_poles_string);
      boost::property_tree::ptree property_tree;
      boost::property_tree::read_json(ss, property_tree);

      for(auto &spins : property_tree)
        {
          result.emplace_back();
          if(!spins.first.empty())
            {
              throw std::runtime_error(
                "Input to user-poles must be an array of arrays of arrays");
            }
          auto &spin(result.back());
          for(auto &degrees : spins.second)
            {
              spin.emplace_back();
              auto &degree(spin.back());
              for(auto &pole : degrees.second)
                {
                  degree.emplace_back(std::string(pole.second.data()));
                }
            }
        }
    }    
  return result;
}
